Usage
=====

* Register a Twitter app, get your Twitter OAuth credentials and put them into your settings.py ::

    SWITTER_CONSUMER_KEY = '...'
    SWITTER_CONSUMER_SECRET = '...'
    SWITTER_ACCESS_TOKEN = '...'
    SWITTER_ACCESS_TOKEN_SECRET = '...'

* Once installed it's pretty straighforward to use cms plugin - just add it yo your pages ::

* That's it! :-)
