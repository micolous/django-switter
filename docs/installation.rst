.. _installation:

Installation
============

* To install ::

    pip install django-switter

* Add ``'switter'`` to your ``INSTALLED_APPS`` setting::

    INSTALLED_APPS = (
        # other apps
        "switter",
        "switter.plugins",  # this one is for django-cms
    )

* Add Switter url patterns to your main urls.py ::

    urlpatterns = patterns('',
        ...

        url(r'^switter/', include('switter.urls')),
        
        ...
    )

* And update your database::

    python manage.py migrate switter
    python manage.py migrate switter.plugins


* Or just ``syncdb`` if you don't use South migrations::

    python manage.py syncdb

