Switter app description
=======================

Switter is a drop-in Django app that gets tweets for you, keeps them cached for a short while and makes it easy to display them on your site:

- **backend** part - fetches and caches tweets from Twitter 1.1 API,
- **frontend** part - is slightly modified version of a popular **jquery.tweet.js** plugin, that uses Switter backend instead of talking directly to Twitter API,
- **django-cms plugin** that makes use of Switter backend and fornend in an easy and pleasant way: when cached tweets are fresh enogh, it doesn't even make any ajax call, they're rendered right away.



Development
-----------

The source repository can be found at https://bitbucket.org/grucha/django-switter


Contents
========

.. toctree::
 :maxdepth: 1

 changelog
 installation
 usage
 customization
 