# urlunquote() for django < 1.4
try:
    from django.utils.http import urlunquote
except ImportError:
    import urllib
    from django.utils.encoding import smart_str, force_unicode

    def urlunquote(quoted_url):
        """
        Copied from django 1.4 for compatibility with older versions.
        A wrapper for Python's urllib.unquote() function that can operate on
        the result of django.utils.http.urlquote().
        """
        return force_unicode(urllib.unquote(smart_str(quoted_url)))

# now() for django < 1.4
try:
    from django.utils.timezone import now
except ImportError:
    from datetime import datetime
    now = datetime.now